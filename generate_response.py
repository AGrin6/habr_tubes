import pickle
import numpy
import research

class FinalModel(object):
  def __init__(self, model, to_sample, additional_data):
    self._model = model
    self._to_sample = to_sample
    self._additional_data = additional_data
  def process(self, instance):
    return self._model.predict(numpy.array(self._to_sample(
                                               instance, self._additional_data)).reshape(1, -1))[0]

if __name__ == '__main__':
  with open('./data/model.mdl', 'rb') as input_stream:
    model = pickle.loads(input_stream.read())
  additional_data = research.load_additional_data()
  final_model = FinalModel(model, research.to_sample, additional_data)
  # print(final_model.process({'tube_assembly_id':'TA-00001', 'supplier':'S-0066',
  #                            'quote_date':'2013-06-23', 'annual_usage':'0',
  #                            'min_order_quantity':'0', 'bracket_pricing':'Yes',
  #                            'quantity':'1'}))
  list_of_predictions = []
  with open('./data/competition_data/test_set.csv') as input_stream:
    header_line = input_stream.readline()
    column_names = header_line[:-1].split(',')
    for line in input_stream:
      cell_values = line[:-1].split(',')
      # new_id = cell_values[column_names.index('id')]
      new_id = cell_values[0] # id column
      new_instance = dict(zip(column_names[1:], cell_values[1:]))
      new_prediction = final_model.process(new_instance)
      list_of_predictions.append((new_id, new_prediction))

  with open('./data/output.csv', 'w') as output_stream:
    output_stream.write('id,cost\n')
    for prediction in list_of_predictions:
      output_stream.write(prediction[0] + ',' + str(prediction[1]) + '\n')

