import sys
import time
import pickle
import operator
import math
import numpy
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import GradientBoostingRegressor

TRAIN_SAMPLES_NUM = 20000

MODEL_SETTINGS = {
  'model_name':'GradientBoostingRegressor',
  'learning_rate':0.550,
  'n_estimators':100,
  'max_depth':3,
  'min_samples_split':4,
  'min_samples_leaf':6,
  'min_weight_fraction_leaf':0.000,
  'subsample':1.000,
  'random_seed':0}


def load_data():
  list_of_instances = []
  list_of_labels =[]

  with open('./data/competition_data/train_set.csv') as input_stream:
    header_line = input_stream.readline()
    columns = header_line.strip().split(',')
    for line in input_stream:
      new_instance = dict(zip(columns[:-1], line.split(',')[:-1]))
      new_label = float(line.split(',')[-1])
      list_of_instances.append(new_instance)
      list_of_labels.append(new_label)
  return list_of_instances, list_of_labels


def is_bracket_pricing(instance):
  if instance['bracket_pricing'] == 'Yes':
    return [1]
  elif instance['bracket_pricing'] == 'No':
    return [0]
  else:
    raise ValueError


def get_quantity(instance):
  return [int(instance['quantity'])]


def get_min_order_quantity(instance):
  return [int(instance['min_order_quantity'])]


def get_annual_usage(instance):
  return [int(instance['annual_usage'])]


def get_absolute_date(instance):
  return [365 * int(instance['quote_date'].split('-')[0])
          + 12 * int(instance['quote_date'].split('-')[1])
          + int(instance['quote_date'].split('-')[2])]


SUPPLIERS_LIST = ['S-0058', 'S-0013', 'S-0050', 'S-0011', 'S-0070', 'S-0104', 'S-0012', 'S-0068', 'S-0041', 'S-0023', 'S-0092', 'S-0095', 'S-0029', 'S-0051', 'S-0111', 'S-0064', 'S-0005', 'S-0096', 'S-0062', 'S-0004', 'S-0059', 'S-0031', 'S-0078', 'S-0106', 'S-0060', 'S-0090', 'S-0072', 'S-0105', 'S-0087', 'S-0080', 'S-0061', 'S-0108', 'S-0042', 'S-0027', 'S-0074', 'S-0081', 'S-0025', 'S-0024', 'S-0030', 'S-0022', 'S-0014', 'S-0054', 'S-0015', 'S-0008', 'S-0007', 'S-0009', 'S-0056', 'S-0026', 'S-0107', 'S-0066', 'S-0018', 'S-0109', 'S-0043', 'S-0046', 'S-0003', 'S-0006', 'S-0097']

def get_supplier(instance):
  if instance['supplier'] in SUPPLIERS_LIST:
    supplier_index = SUPPLIERS_LIST.index(instance['supplier'])
    result = [0] * supplier_index + [1] + [0] * (len(SUPPLIERS_LIST) - supplier_index - 1)
  else:
    result = [0] * len(SUPPLIERS_LIST)
  return result


def get_assembly(instance):
  assembly_id = int(instance['tube_assembly_id'].split('-')[1])
  result = [0] * assembly_id + [1] + [0] * (25000 - assembly_id - 1)
  return result


def get_assembly_specs(instance, assembly_to_specs):
  result = [0] * 100
  for spec in assembly_to_specs[instance['tube_assembly_id']]:
    result[int(spec.split('-')[1])] = 1
  return result


def get_assembly_components(instance, assembly_to_components,
                            components_by_popularity, number_of_components):
  """
  number_of_components: number of most popular components taken into account
  """
  result = [0] * number_of_components
  for component in sorted(assembly_to_components[instance['tube_assembly_id']]):
    component_index = components_by_popularity.index(component)
    if component_index < number_of_components:
      # quantity
      result[component_index] = assembly_to_components[instance['tube_assembly_id']][component]
  return result


MATERIALS_LIST = ['NA', 'SP-0008', 'SP-0019', 'SP-0028', 'SP-0029', 'SP-0030', 'SP-0031', 'SP-0032', 'SP-0033', 'SP-0034', 'SP-0035', 'SP-0036', 'SP-0037', 'SP-0038', 'SP-0039', 'SP-0041', 'SP-0044', 'SP-0045', 'SP-0046', 'SP-0048']

def get_material(instance, assembly_to_material):
  material = assembly_to_material[instance['tube_assembly_id']]
  if material in MATERIALS_LIST:
    material_index = MATERIALS_LIST.index(material)
    result = [0] * material_index + [1] + [0] * (len(MATERIALS_LIST) - material_index - 1)
  else:
    result = [0] * len(MATERIALS_LIST)
  return result


def get_diameter(instance, assembly_to_diameter):
  return [assembly_to_diameter[instance['tube_assembly_id']]]


def to_sample(instance, additional_data):
  return (is_bracket_pricing(instance) + get_quantity(instance) + get_min_order_quantity(instance)
          + get_annual_usage(instance) + get_absolute_date(instance) + get_supplier(instance)
          + get_assembly_specs(instance, additional_data['assembly_to_specs'])
          + get_assembly_components(instance, additional_data['assembly_to_components'],
                                    additional_data['components_by_popularity'], 100)
          + get_diameter(instance, additional_data['assembly_to_diameter'])
          )
          # + get_material(instance, additional_data['assembly_to_material'])


def to_interim_label(label):
  return math.log(label + 1)


def to_final_label(interim_label):
  return math.exp(interim_label) - 1


def load_additional_data():
  result = dict()

  assembly_to_material = dict()
  assembly_to_diameter = dict()
  with open('./data/competition_data/tube.csv') as input_stream:
    header_line = input_stream.readline()
    for line in input_stream:
      tube_assembly_id = line.split(',')[0]
      material_id = line.split(',')[1]
      diameter = float(line.split(',')[2])
      assembly_to_material[tube_assembly_id] = material_id
      assembly_to_diameter[tube_assembly_id] = diameter
  result['assembly_to_material'] = assembly_to_material
  result['assembly_to_diameter'] = assembly_to_diameter

  assembly_to_specs = dict()
  with open('data/competition_data/specs.csv') as input_stream:
    header_line = input_stream.readline()
    for line in input_stream:
      tube_assembly_id = line.split(',')[0]
      specs = []
      for spec in line.strip().split(',')[1:]:
        if spec != 'NA':
          specs.append(spec)
      assembly_to_specs[tube_assembly_id] = specs
  result['assembly_to_specs'] = assembly_to_specs

  assembly_to_components = dict()
  component_to_popularity = dict()
  with open('./data/competition_data/bill_of_materials.csv') as input_stream:
    header_line = input_stream.readline()
    for line in input_stream:
      tube_assembly_id = line.split(',')[0]
      assembly_to_components[tube_assembly_id] = dict()
      for i in range(1, 16, 2):
        new_component = line.split(',')[i]
        if new_component != 'NA':
          quantity = int(line.split(',')[i + 1])
          assembly_to_components[tube_assembly_id][new_component] = quantity
          if new_component in component_to_popularity:
            component_to_popularity[new_component] += 1
          else:
            component_to_popularity[new_component] = 1
  components_by_popularity = [value[0] for value in sorted(component_to_popularity.items(),
                                                      key=operator.itemgetter(1, 0), reverse=True)]
  result['assembly_to_components'] = assembly_to_components
  result['components_by_popularity'] = components_by_popularity
  # print(result.keys())
  # str_output = str('\n'.join([str([key, assembly_to_components[key]])
  #                  for key in sorted(assembly_to_components.keys())]))
  # with open('./0', 'w') as output_stream:
  #   output_stream.write(str_output)
  # print(str_output[:1000])
  # # print(str(components_by_popularity))
  # sys.exit()
  return result


if __name__ == '__main__':
  list_of_instances, list_of_labels = load_data()
  # print(len(list_of_instances), len(list_of_labels))
  # print(list_of_instances[:3])
  # print(list_of_labels[:3])
  # print(list(map(to_sample, list_of_instances[:3])))

  additional_data = load_additional_data()
  # print(additional_data['assembly_to_components']['TA-00001'])
  # print(additional_data['components_by_popularity'][:3])
  # sys.exit()
  # print(additional_data)

  # print(to_final_label(to_interim_label(42)))

  list_of_samples = list(map(lambda x:to_sample(x, additional_data), list_of_instances))
  train_samples = list_of_samples[:TRAIN_SAMPLES_NUM]
  train_labels = list(map(to_interim_label, list_of_labels[:TRAIN_SAMPLES_NUM]))


  # from keras.models import Sequential
  # from keras.layers import Dense
  # from keras.optimizers import SGD
  # model = Sequential()
  # model.add(Dense(units=30, activation='tanh', input_dim=len(list_of_samples[0])))
  # model.add(Dense(units=20, activation='tanh', input_dim=30))
  # model.add(Dense(units=1, activation='linear'))
  # optimizer = SGD(lr=0.1)
  # model.compile(loss='mean_squared_error', optimizer=optimizer, metrics=['accuracy'])

  model = GradientBoostingRegressor(
                         learning_rate=MODEL_SETTINGS['learning_rate'],
                         n_estimators=MODEL_SETTINGS['n_estimators'],
                         max_depth=MODEL_SETTINGS['max_depth'],
                         min_samples_split=MODEL_SETTINGS['min_samples_split'],
                         min_samples_leaf=MODEL_SETTINGS['min_samples_leaf'],
                         random_state=MODEL_SETTINGS['random_seed'],
                         min_weight_fraction_leaf=MODEL_SETTINGS['min_weight_fraction_leaf'],
                         subsample=MODEL_SETTINGS['subsample'])
  # model = LinearRegression() # RandomForestRegressor RidgeRegressor
  time_start = time.time()
  model.fit(numpy.array(train_samples), numpy.array(train_labels))
  print('Time spent: {0}'.format(time.time() - time_start))

  validation_samples = list_of_samples[TRAIN_SAMPLES_NUM:]
  validation_labels = list(map(to_interim_label, list_of_labels[TRAIN_SAMPLES_NUM:]))

  squared_errors = []
  for sample, label in zip(validation_samples, validation_labels):
    prediction = model.predict(numpy.array(sample).reshape(1, -1))[0]
    squared_errors.append((prediction - label) ** 2)
    
  mean_squared_error = math.sqrt(sum(squared_errors) / len(squared_errors))
  print('MODEL_SETTINGS = {{\n  \'model_name\': {0},\n  \'learning_rate\': {1},\n'
        '  \'n_estimators\': {2},\n  \'max_depth\': {3},\n  \'min_samples_split\': {4},\n'
        '  \'min_samples_leaf\': {5},\n  \'random_seed\': {6},\n'
        '  \'min_weight_fraction_leaf\': {7},\n  \'subsample\': {8}}}'.format(
                MODEL_SETTINGS['model_name'], MODEL_SETTINGS['learning_rate'],
                MODEL_SETTINGS['n_estimators'], MODEL_SETTINGS['max_depth'],
                MODEL_SETTINGS['min_samples_split'], 
                MODEL_SETTINGS['min_samples_leaf'], MODEL_SETTINGS['random_seed'],
                MODEL_SETTINGS['min_weight_fraction_leaf'], MODEL_SETTINGS['subsample']))
  print('Mean Squared Error: {0}'.format(mean_squared_error))

  with open('./data/model.mdl', 'wb') as output_stream:
    output_stream.write(pickle.dumps(model))

